package com.investree.demo.repository;

import com.investree.demo.model.Transaksi;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface TransaksiRepository extends CrudRepository<Transaksi,Long>, PagingAndSortingRepository<Transaksi, Long> {
}
