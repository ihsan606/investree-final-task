package com.investree.demo.repository;

import com.investree.demo.model.PaymentHistory;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PaymentHistoryRepository extends CrudRepository<PaymentHistory, Long>, PagingAndSortingRepository<PaymentHistory,Long> {
}
