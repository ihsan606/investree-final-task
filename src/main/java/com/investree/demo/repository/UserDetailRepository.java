package com.investree.demo.repository;

import com.investree.demo.model.UserDetail;
import org.springframework.data.repository.CrudRepository;

public interface UserDetailRepository extends CrudRepository<UserDetail,Long> {
}
